const path = require('path')
const fs = require('fs')
const puppeteer = require('puppeteer')
const ejs = require('ejs')

const url = urlValidator(process.argv[2])

const iconPaths = {
  'Apt/Condo': './assets/apartment.svg',
  Townhouse: './assets/townhouse-icon.svg',
  House: './assets/home-white.svg',
}
;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto(url)
  await page.waitForSelector('.summarybar--property')

  const [price, address, [bed, bath, square, type]] = await Promise.all([
    getPrice(page),
    getAddress(page),
    getProperty(page),
  ])

  console.log('price:', price)
  console.log('address:', address)
  console.log('bed:', bed)
  console.log('bath:', bath)
  console.log('square:', square)
  console.log('type:', type)

  const htmlStr = await getTemplate({
    price,
    address,
    type,
    bed,
    bath,
    square,
    iconPath: iconPaths[type],
  })

  writeToFile(htmlStr)

  await page.goto(`file:${path.join(__dirname, 'tmp.html')}`)
  await page.screenshot({
    path: `./dist/${address}.jpg`,
    fullPage: true,
    type: 'jpeg',
    quality: 100,
  })
  await browser.close()

  console.log('\nDone!')
})()

function urlValidator(urlToValidate) {
  if (!urlToValidate.includes('www.rew.ca/properties')) {
    throw new Error('Invalide URL: url is expected to contain "www.rew.ca"')
  }
  return urlToValidate
}

function getTemplate(data) {
  return new Promise((resolve) => {
    ejs.renderFile(
      path.join(__dirname, 'template.ejs'),
      data,
      {},
      (err, str) => {
        if (err) throw new Error(err)
        resolve(str)
      }
    )
  })
}

function getPrice(page) {
  return page.evaluate(
    () => document.querySelector('.propertyheader-price').textContent
  )
}

function getAddress(page) {
  return page.evaluate(
    () => document.querySelector('.propertyheader-address').textContent
  )
}

function getProperty(page) {
  // Return: [ '1 Bed', '1 Bath', '704 Sqft', 'Apt/Condo' ]
  return page.evaluate(() =>
    [...document.querySelectorAll('.summarybar-item')].map((element) =>
      element.textContent
        .replace(/\n+/g, ' ')
        .replace('Type', '')
        .trim()
    )
  )
}

function writeToFile(html) {
  fs.writeFileSync(path.join(__dirname, 'tmp.html'), html, 'utf8')
}
